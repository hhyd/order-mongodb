package com.lcy.order.controller;


import com.lcy.order.pojo.Logistics;
import com.lcy.order.pojo.Order;
import com.lcy.order.service.OrderService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.Map;

@RestController
@RequestMapping("order")
public class OrderController {
    @Resource
    private OrderService orderService; //注入orderService对象

    /***
     * 创建订单
     * @param order
     * @return
     */
    @PostMapping("add")
    public String addOrder(Order order) {
        orderService.addOrder(order);
        return "订单创建成功！";
    }

    /***
     * 更新订单信息
     * 追加物流信息
     * @param logistics
     * @return
     */
    @PostMapping("update")
    public String updateOrder(Logistics logistics) {
        if(logistics.getOrderId()!= null &&  logistics.getOrderId()!= "" ) {
            orderService.updateOrderAndLogistics(logistics);
            return "物流信息追加成功！";
        }else{
            return "物流编号不能为空！";
        }
    }
    /***
     *根据订单编号来查询
     * @param id
     * @return
     */
    @GetMapping("{id}")
    public Order selecetOrder(@PathVariable String id) {
        if(id!=null && id!="") {
            return orderService.selectOrderById(id);
        }else{
            return null;
        }
    }
    /**
     * 查询所有订单
     * @return
     */
    @GetMapping("list")
    public Map<String, Object> selectOrderList(){
        return orderService.selectOrderList();
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @PostMapping("delete")
    public String deleteOrderById(String id) {
       boolean rst= orderService.deleteOrderById(id);
       if (rst) {
           return "订单删除成功！";
       }else{
            return "订单删除失败！";
           }
    }
}